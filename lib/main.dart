import 'package:flutter/material.dart';
import 'package:hidden_drawer_menu/hidden_drawer_menu.dart';

main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyApp();
  }
}

class _MyApp extends State<MyApp> {
  // Buat list ScreenHiddenDrawer
  List<ScreenHiddenDrawer> items = List();
  // Add items di init() method
  @override
  void initState() {
    items.add(new ScreenHiddenDrawer(
        new ItemHiddenMenu(
          name: "Home",
          baseStyle: TextStyle(color: Colors.white.withOpacity(0.8), fontSize: 28.0),
          colorLineSelected: Colors.teal,
        ),
        HomeScreen()));
    items.add(new ScreenHiddenDrawer(
        new ItemHiddenMenu(
          name: "Gallery",
          baseStyle: TextStyle(color: Colors.white.withOpacity(0.8), fontSize: 28.0),
          colorLineSelected: Colors.orange,
        ),
        GalleryScreen()));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HiddenDrawerMenu(
        initPositionSelected: 0,
        screens: items,
        contentCornerRadius: 30.0,
        backgroundColorMenu: Colors.cyan,
        backgroundMenu: DecorationImage(
            image: NetworkImage(
              'https://cdn.pixabay.com/photo/2017/08/07/21/52/nature-2608274_1280.jpg',
            ),
            fit: BoxFit.cover),
        //    typeOpen: TypeOpen.FROM_RIGHT,
        //    enableScaleAnimin: true,
        //    enableCornerAnimin: true,
        //    slidePercent: 80.0,
        //    verticalScalePercent: 80.0,
        //    iconMenuAppBar: Icon(Icons.menu),
        //    backgroundContent: DecorationImage((image: ExactAssetImage('assets/bg_news.jpg'),fit: BoxFit.cover),
        //    whithAutoTittleName: true,
        //    styleAutoTittleName: TextStyle(color: Colors.red),
        //    actionsAppBar: <Widget>[],
        //    backgroundColorContent: Colors.blue,
        //    backgroundColorAppBar: Colors.blue,
        //    elevationAppBar: 1.0,
        //    tittleAppBar: Center(child: Icon(Icons.ac_unit),),
        //    enableShadowItensMenu: true,
      ),
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "Home Screen",
              style: TextStyle(fontSize: 30.0),
            ),
          ],
        ),
      ),
    );
  }
}

class GalleryScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "Gallery Screen",
              style: TextStyle(fontSize: 30.0),
            ),
            RaisedButton(
              child: Text('Buka Menu'),
              onPressed: () {
                SimpleHiddenDrawerController.of(context).toggle();
              },
            )
          ],
        ),
      ),
    );
  }
}
